var b =10;
(function b(){
    b = 20;
    console.log(b); 
})();

//简单改造上面的代码，使之分别打印出10和20

// 打印出10
var b =10;
(function b(){
    b = 20;
    console.log(this.b); 
})();

var b =10;
(function b(){
    b = 20;
    console.log(window.b); 
})();

var b = 10;
(function b(b){
   console.log(b); 
   b = 20;
})(b);



//打印出20

var b =10;
(function a(){
    b = 20;
    console.log(b); 
})();

var b = 10;
(function b(b){
    b = 20;
   console.log(b); 
})(b);

//同时打出10 和 20
var b = 10;
(function b(b){
    console.log(window.b);//10
    b = 20;
    console.log(b); //20
})();

var b = 10;
 (function (c){
   b = 20;
  console.log(b); //20
  console.log(c); //10
})(b);