// 一个普通函数
function getData(){
    return "syy"
}
//syy
console.log(getData());

// 加上async后
async function getData(){
    return "syy"
}
//Promise {<fulfilled>: 'syy'}
console.log(getData());

async function getData(){
    return "syy"
}
getData().then(data=>{
    console.log(data);//syy
})


async function getData(){
    throw new Error("出错了！")
}
getData().then(v=>{
    console.log(v);
}).catch(e=>{
    // Error: 出错了！
    console.log(e);
})

//https://blog.csdn.net/weixin_43586120/article/details/100536903
//https://blog.csdn.net/qq_52654932/article/details/130373910