//获取随机布尔值
const randomBoolean = () => Math.random() >= 0.5;
randomBoolean()
//反转字符串
const reverseStr = (str) => str.split('').reverse().join('');
reverseStr("hello")
//数组去重
const removeDuplicates = (arr) => [...new Set(arr)];
removeDuplicates(['foo', 'bar', 'bar', 'foo', 'bar'])
//判断数字奇偶
const isEven = (num) => num % 2 === 0;
isEven(5)
//获取日期对象的时间部分
const timeFromDate = (date) => date.toTimeString().slice(0, 8);
timeFromDate(new Date(2021, 0, 10, 17, 30, 0))
//判断是否为Apple设备
const isAppleDevice = /Mac|iPod|iPhone|iPad/.test(navigator.platform)
isAppleDevice
//滚动到页面顶部
const goToTop = () => window.scrollTo(0, 0);
goToTop()
//求平均值
const average = (...args) => args.reduce((a, b) => a + b) / args.length;
average(1, 2, 3)