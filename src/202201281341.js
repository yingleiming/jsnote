// 数组去重 数组字符串
//去重前 ['1','2','3','1','4','5']
//去重后 ['1','2','3','4','5']

let val = ['1', '2', '3', '1', '4', '5']
function unique (arr) {
  let hash = []
  for (var i = 0; i < arr.length; i++) {
    if (hash.indexOf(arr[i]) == -1) {
      hash.push(arr[i])
    }
  }
  return hash
}

let newval = unique(val)
console.log(newval)//['1','2','3','4','5']