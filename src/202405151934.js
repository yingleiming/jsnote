//1.导入 fs 文件系统模块
const fs = require('fs')

//2.调用fs.writFile()方法，写入文件的内容
// 参数 1:表示文件的存放路径；
// 参数 2：表示文件写人的内容；
// 参数 3：回掉函数；
fs.writeFile('./b.txt','hello node.js',function(err){
  //2.1 如果文件写入成功，则 err 的值等于null；
  //2.2 如果文件写入失败，则 err 的值等于一个错误对象；
  if (err){
    return console.log('文件写入失败',err.message)
  }
  console.log('文件写入成功')
})