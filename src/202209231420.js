//new Promise被实例化之后会立即执行
let p = new Promise((resolve,reject)=>{
  console.log('77777')
})

//输出结果 77777

//返回的是一个Promise对象
console.log(p)

//输出结果 Promise {<pending>}

