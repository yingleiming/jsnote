const path = require('path')

//定义文件的存放路径
let fullname='a/b/c/d/e/f/index.html'

let fpath = path.basename(fullname)
console.log(fpath)//输出：index.html


let nameWithoutExt = path.basename(fpath,'.html')
console.log(nameWithoutExt)//输出：index


let extname = path.extname(fullname)
console.log(extname)//输出：.html




// mockjs随机获取整数元素数组
const Mock = require('mockjs')
const Random = Mock.Random

let arr = []
for (let i = 0; i < 10; i++) {
  arr.push(Random.integer(0, 100))
}