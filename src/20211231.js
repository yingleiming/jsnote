// 问题描述：
// index.js
export const test = () => {
  let count = 1
  return () => {
    console.log(count++)
  }
}

// index.vue
import { test } from './index.js'

test()  // 没有任何打印


// test.js
import { test } from './index.js'

test()
test()

/*
如果是在js中调用结果是 1 2 （错误）

分析：
在index.vue test.js 中都不会执行；
因为在调用test（） 的时候得到的结果是内部的方法，只有再次调用，才能执行内部方法
*/
var fn = test()
fn() // 1
fn() // 2
