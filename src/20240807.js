/**
 * 这个问题要求你实现防抖 debounce 高阶函数。在调用防抖函数后，提供的函数应该在一定延迟 t 后被调用。然而，如果在 t 毫秒内再次调用防抖函数，提供的函数的执行应该被取消并重置计时器。
 *
 *
 *
 *
 *
 *
 *
 * 防抖重在清零，节流中在加锁
 *
 *
 *
 *
 *
 */

var debounce = function(fn,t){
  let timer = null
  return function(...args){
    clearTimeout(timer)
    timer = setTimeout(()=>fn.apply(this,args),t)
  }
}


const start = Date.now()
function log(){
  console.log(Date.now()-start)
}

setTimeout(log,10);
setTimeout(log,20);
setTimeout(log,50);
setTimeout(log,60);