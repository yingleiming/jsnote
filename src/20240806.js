//检查是否是类的对象实例
//编写一个函数，检查给定的值是否是给定类或超类的的实例。
//可以传递给函数的数据类型没有限制。例如，值或类可能是 undefined
//本题要求在 instanceof 的基础上支持基本数据类型
//typeof
//instanceof
//constructor
//getPropertyOf
//递归

class Animal {
  constructor(name,sex,height) {
    this.name = name
    this.sex = sex
    this.height = height
  }

  eat() {
    console.log(this.name,this.sex,this.height,'Animal is eating')
  }
}
const s1 = new Animal('旺旺','女',25)

var checkIfInstanceOf = function (obj, classFunction) {
  if (
    obj === null ||
    obj === undefined ||
    classFunction === null ||
    classFunction === undefined
  ) {
    return false
  }
  return (
    Object.getPrototypeOf(obj) === classFunction.prototype ||
    checkIfInstanceOf(Object.getPrototypeOf(obj), classFunction)
  )
}
checkIfInstanceOf(s1,Animal)