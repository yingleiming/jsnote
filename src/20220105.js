// JS中函数的this指向其调用者
function fn1 () {
  console.log(this)
}
fn1();//第一次调用fn1:window

class People {
  excutor (fn1) {
    function fn2 () {
      console.log(this)
    }
    fn1();//第二次调用fn1:window
    console.log(this);//People
    fn2();//undefined
  }
}

let p = new People();
p.excutor(fn1);

