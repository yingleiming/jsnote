function getNowFormatDate () {
  var date = new Date()
  var decollator = '-'
  var seperator = ':'
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var currentDate = year + decollator + month + decollator + day + ' ' +
    date.getHours() +
    seperator +
    date.getMinutes() +
    seperator +
    date.getSeconds()
  if (month >= 1 && month <= 9) {
    month = '0' + month
  }
  if (day >= 1 && day <= 9) {
    day = '0' + day
  }
  return currentDate
}
getNowFormatDate()