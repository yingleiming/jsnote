/**
 * 怎么判断一个数组里是否嵌套了子数组
 * 怎么判断一个数组是不是双层呢，判断后返回true或false
 * 比如[1,2,3]返回false,[1,[2,2,2],3]返回true,
 * 如果是三层呢，怎么判断
 * 参考 https://segmentfault.com/q/1010000041043956?utm_source=sf-homepage
 *
 *
 */

function isDeepThen(arr) {
  let level = 0
  if (!Array.isArray(arr)) {
    return false
  }
  arr.map(function (item) {
    if (!Array.isArray(item)) {
      level = 1
    }
  })
  return level
}

let a = [1, 2, 3]
isDeepThen(a)