var array = [
  { 'name': 'aiai', 'age': 18 },
  { 'name': 'sisi', 'age': 18 },
  { 'name': 'lulu', 'age': 18 },
  { 'name': 'sisi', 'age': 19 }
]
//查找符合条件值并存入新数组
let newArray = array.filter((item) => {
  return item.age === 18
})
console.log(newArray)

// find()
let arr = [2, 5, 8, 1, 4, 17, 89]
let res = arr.find(item => {
  return item > 9
})
console.log(res)
