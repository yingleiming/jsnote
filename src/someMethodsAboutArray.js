var arr1 = [1, 2, 3, 4, 5]
var arr2 = [2, 3, 4, 7, 8]
var arr3 = [0, 1, 2, 3, 4, 5]
var arr4 = [0, 4, 6, 1, 3, 9]
var arr = []

// 找出两个数组中不相同的元素
function getArrDifference (arr1, arr2) {
  return arr1.concat(arr2).filter(function (v, i, arr) {
    return arr.indexOf(v) === arr.lastIndexOf(v)
  })
}
//输出：(4) [2, 5, 6, 9]
console.log(getArrDifference(arr1, arr2))

// 找出两个数组中相同的元素
var arr3 = [0, 1, 2, 3, 4, 5, 3, 2, 1]
var arr4 = [0, 4, 6, 1, 3, 9]
function getArrEqual (firstArr, secondArr) {
  let newArr = []
  for (let i = 0; i < firstArr.length; i++) {
    for (let j = 0; j < secondArr.length; j++) {
      if (secondArr[j] === firstArr[i]) {
        newArr.push(secondArr[j])
      }
    }
  }
  return newArr
}
console.log(getArrEqual(arr3, arr4))
//输出：(4) [0, 4, 1, 3]

var arr3 = [0, 1, 2, 3, 4, 5, 3, 2, 1]
var arr4 = [0, 4, 6, 1, 3, 9]
let res = arr3.filter(function (n) {
  return arr4.indexOf(n) !== -1
})
function unique (arr) {
  if (!Array.isArray(arr)) {
    console.log('type error!')
    return
  }
  var array = []
  for (var i = 0; i < arr.length; i++) {
    if (!array.includes(arr[i])) { //includes 检测数组是否有某个值
      array.push(arr[i])
    }
  }
  return array
}
console.log(unique(res))